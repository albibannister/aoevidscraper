# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import cv2
import pytesseract

pytesseract.pytesseract.tesseract_cmd = r'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'

vid = cv2.VideoCapture('hc3.mp4')
vid.set(cv2.CAP_PROP_POS_FRAMES,5000)

width  = vid.get(cv2.CAP_PROP_FRAME_WIDTH)  # float
height = vid.get(cv2.CAP_PROP_FRAME_HEIGHT) # float



resBoxWidth = (int)(width*0.269531)
resBoxHeight = (int)(height*0.0555555556)
resCellWidth = resBoxWidth//4
resTextWidth = (int)(width*0.03125)
resTextHeight = (int)(height*0.03194444)
vilTextWidth = 17
resEdge = (int)(width*0.0234375) 

def grayScaleInvert(img):
    temp = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    return cv2.bitwise_not(temp)

def enlargeImg(img,scale):
    scale_percent = scale # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    return cv2.resize(img, dim, interpolation = cv2.INTER_AREA) 

def getResCountFromCell(cell):
    return enlargeImg(cell[0:resBoxHeight//2,resTextWidth:resCellWidth],200) 

# =============================================================================
# Takes a cropped image of one player's resources and extracts the individual resource
# counts as a tuple in order food, wood, gold, stone
# =============================================================================
def getResourceCounts(resInfoImg):
    woodCell = resInfoImg[0:resBoxHeight,0:resCellWidth]
    foodCell = resInfoImg[0:resBoxHeight,resCellWidth:resCellWidth*2]
    goldCell = resInfoImg[0:resBoxHeight,resCellWidth*2:resCellWidth*3]
    stoneCell = resInfoImg[0:resBoxHeight,resCellWidth*3:resCellWidth*4]
    
    woodCountImg = getResCountFromCell(woodCell)
    foodCountImg = getResCountFromCell(foodCell)
    goldCountImg = getResCountFromCell(goldCell)
    stoneCountImg = getResCountFromCell(stoneCell)  
    
    woodCount = pytesseract.image_to_string(woodCountImg,config = '--psm 7')
    foodCount = pytesseract.image_to_string(foodCountImg,config='--psm 7')
    goldCount = pytesseract.image_to_string(goldCountImg)
    stoneCount = pytesseract.image_to_string(stoneCountImg,config='--psm 7')
    return woodCount,foodCount,goldCount,stoneCount

def outputResCount(resCount):
    print("food: {} wood: {} gold: {} stone: {}".format(resCount[0],resCount[1],resCount[2],resCount[3]))
    
    
for i in range(0,20):
    success, frame = vid.read()
    
    resInfo = frame[1:resBoxHeight,resEdge:resEdge + resBoxWidth]
    p2resInfo = frame[1:resBoxHeight,(int)(width-resEdge-resBoxWidth):(int)(width-resEdge)]
    
    resInfo = grayScaleInvert(resInfo)
    p2resInfo = grayScaleInvert(p2resInfo)  
    
    #cv2.imshow('p1res',resInfo)
    #cv2.imshow('p2res',p2resInfo)
    
    p1ResCount = getResourceCounts(resInfo)
    p2ResCount = getResourceCounts(p2resInfo)
    
    print("frame: {}".format(i))
    print("p1 resources:")
    outputResCount(p1ResCount)  
    print("p2 resources:") 
    outputResCount(p2ResCount)

cv2.waitKey(0)





